<?php

namespace App\Service;

use App\Entity\Venue;
use App\Repository\VenueRepository;

class VenueCrud
{
    private VenueRepository $venueRepository;

    public function __construct(VenueRepository $venueRepository)
    {
        $this->venueRepository = $venueRepository;
    }

    public function read(int $id): ?Venue
    {
        return $this->venueRepository->find($id);
    }
}