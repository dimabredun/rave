<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public static function createApprovedCriteria(): Criteria
    {
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq('status', Comment::STATUS_APPROVED));
    }

    /**
     * @return Comment[]
     */
    public function findMostPopular(string $search = null): array
    {
        $queryBuilder = $this->createQueryBuilder('comment')
            ->addCriteria(self::createApprovedCriteria())
            ->orderBy('comment.votes', 'DESC')
            ->innerJoin('comment.party', 'party')
            ->addSelect('party');

        // We've split these query on two parts, because want to make sure, that we will apply the search logic only
        // if the search logic will passed

        if ($search) {
            $queryBuilder->andWhere('comment.content LIKE :searchTerm OR party.party LIKE :searchTerm')
                ->setParameter('searchTerm', '%'.$search.'%');
        }

        return $queryBuilder
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}
