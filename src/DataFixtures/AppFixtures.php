<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Party;
use App\Entity\Tag;
use App\Factory\CommentFactory;
use App\Factory\PartyFactory;
use App\Factory\TagFactory;
use App\Factory\UserFactory;
use App\Factory\VenueFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $venues = VenueFactory::createMany(10);

        TagFactory::createMany(100);

        PartyFactory::createMany(20, function () use($venues) {
            return [
                'venue' => $venues[array_rand($venues)],
                'tags' => TagFactory::randomRange(1, 5),
            ];
        });

        UserFactory::createOne(['email' => 'testemail@test.com']);
        $users = UserFactory::createMany(10);

        CommentFactory::createMany(100, function () use($users) {
            return [
                'party' => PartyFactory::random(),
                'user' => $users[array_rand($users)],
            ];
        });

//        $parties = PartyFactory::createMany(20, function () {
//            return [
//                'tags' => TagFactory::random(),
//                'venue' => VenueFactory::random(),
//            ];
//        });

//        VenueFactory::createMany(10, function($parties) {
//            return [
//                'parties' => PartyFactory::random()
//            ];
//        });



//        CommentFactory::createMany(100, function () use ($parties) {
//            return [
//                'party' => $parties[array_rand($parties)],
//                'users' => UserFactory::random(),
//            ];
//        });

//        CommentFactory::new(function () use ($parties) {
//            return [
//                'party' => $parties[array_rand($parties)],
//            ];
//        })->needsApproval()->many(20)->create();

        $manager->flush();
    }
}
