<?php

namespace App\Factory;

use App\Entity\Venue;
use App\Repository\VenueRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Venue>
 *
 * @method static Venue|Proxy createOne(array $attributes = [])
 * @method static Venue[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Venue|Proxy find(object|array|mixed $criteria)
 * @method static Venue|Proxy findOrCreate(array $attributes)
 * @method static Venue|Proxy first(string $sortedField = 'id')
 * @method static Venue|Proxy last(string $sortedField = 'id')
 * @method static Venue|Proxy random(array $attributes = [])
 * @method static Venue|Proxy randomOrCreate(array $attributes = [])
 * @method static Venue[]|Proxy[] all()
 * @method static Venue[]|Proxy[] findBy(array $attributes)
 * @method static Venue[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Venue[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static VenueRepository|RepositoryProxy repository()
 * @method Venue|Proxy create(array|callable $attributes = [])
 */
final class VenueFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->title(),
            'address' => self::faker()->name(),
            'genre' => self::faker()->word(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Venue $venue): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Venue::class;
    }
}
