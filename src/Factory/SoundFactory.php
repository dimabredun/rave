<?php

namespace App\Factory;

use App\Entity\Sound;
use App\Repository\SoundRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Sound>
 *
 * @method static Sound|Proxy createOne(array $attributes = [])
 * @method static Sound[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Sound|Proxy find(object|array|mixed $criteria)
 * @method static Sound|Proxy findOrCreate(array $attributes)
 * @method static Sound|Proxy first(string $sortedField = 'id')
 * @method static Sound|Proxy last(string $sortedField = 'id')
 * @method static Sound|Proxy random(array $attributes = [])
 * @method static Sound|Proxy randomOrCreate(array $attributes = [])
 * @method static Sound[]|Proxy[] all()
 * @method static Sound[]|Proxy[] findBy(array $attributes)
 * @method static Sound[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Sound[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static SoundRepository|RepositoryProxy repository()
 * @method Sound|Proxy create(array|callable $attributes = [])
 */
final class SoundFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->userName(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Sound $sound): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Sound::class;
    }
}
