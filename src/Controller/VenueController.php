<?php

namespace App\Controller;

use App\Repository\VenueRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VenueController extends AbstractController
{
    #[Route('/venue', name: 'app_venue')]
    public function allVenues(VenueRepository $repository): Response
    {
        $queryBuilder = $repository->createDataQueryBuilder();

        $pagerFanta = new Pagerfanta(
            new QueryAdapter($queryBuilder)
        );

        $pagerFanta->setMaxPerPage(5);

        return $this->render('venue/venue.html.twig', [
            'pager' => $pagerFanta,
        ]);
    }
}
