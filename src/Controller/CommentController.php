<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/comments/{id}/vote", methods="POST", name="comment_vote")
     */
    public function commentVote(Comment $comment, LoggerInterface $logger, Request $request, EntityManagerInterface $entityManager)
    {
        $data = json_decode($request->getContent(), true);
        $direction = $data['direction'] ?? 'up';

        // use real logic here to save this to the database
        if ($direction === 'up') {
            $logger->info('Voting up!');
            $comment->setVotes($comment->getVotes() + 1);
        } else {
            $logger->info('Voting down!');
            $comment->setVotes($comment->getVotes() - 1);
        }

        $entityManager->flush();

        return $this->json(['votes' => $comment->getVotes()]);
    }

    #[Route('/comments/popular', name: 'app_popular_comments')]
    public function popularComments(CommentRepository $commentRepository, Request $request)
    {
        $comments = $commentRepository->findMostPopular(
            $request->query->get('q')
        );

        return $this->render('comment/popularComments.html.twig',[
            'comments' => $comments
        ]);
    }
}