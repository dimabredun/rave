<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Party;
use App\Repository\CommentRepository;
use App\Repository\PartyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeedController extends AbstractController
{
    #[Route('/{page<\d+>}', name: 'app_homepage')]
    public function homepage(PartyRepository $repository, int $page = 1): Response
    {
        $queryBuilder = $repository->createSelectedOrderedByNewestQueryBuilder();

        $pagerFanta = new Pagerfanta(
            new QueryAdapter($queryBuilder)
        );

        $pagerFanta->setMaxPerPage(5);
        $pagerFanta->setCurrentPage($page);

        return $this->render('homepage.html.twig', [
            'pager' => $pagerFanta,
        ]);
    }

    #[Route('/party/new-party', name: 'app_party_new')]
    public function new(EntityManagerInterface $entityManager): Response
    {
        return new Response('We will recreate this page in a future tutorial');

//        return new Response(sprintf(
//            'Here we go with paeties and sprintf practise! Party #%d called %s',
//            $party->getId(),
//            $party->getName(),
//        ));
    }

    #[Route('/party/{slug}', name: 'app_party_view')]
    public function show(Party $party): Response
    {
//        $comments = $party->getComments();
//        foreach ($comments as $comment) {
//            dump($comment);
//        }

        return $this->render('party.html.twig', [
            'party' => $party,
//            'comments' => $comments,
        ]);
    }

    #[Route('party/{slug}/vote', name: 'app_party_vote', methods: 'POST')]
    public function partyVote(Party $party, Request $request, EntityManagerInterface $entityManager)
    {
        $direction = $request->request->get('direction');

        if($direction === 'up') {
            $party->setVotes($party->getVotes() +1);
        } elseif ($direction === 'down') {
            $party->setVotes($party->getVotes() -1);
        }

        $entityManager->flush();

        return $this->redirectToRoute('app_party_view', [
            'slug' => $party->getSlug()
        ]);
    }
}