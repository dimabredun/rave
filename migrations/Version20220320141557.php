<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220320141557 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, party_id INT NOT NULL, user_id INT DEFAULT NULL, content LONGTEXT NOT NULL, votes INT NOT NULL, username VARCHAR(255) NOT NULL, status VARCHAR(15) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9474526C213C1059 (party_id), INDEX IDX_9474526CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE party (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, date DATETIME NOT NULL, headliner VARCHAR(255) DEFAULT NULL, price INT NOT NULL, slug VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, votes INT NOT NULL, venue INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE party_tag (party_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_7ACB0A50213C1059 (party_id), INDEX IDX_7ACB0A50BAD26311 (tag_id), PRIMARY KEY(party_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smpost (id INT AUTO_INCREMENT NOT NULL, link VARCHAR(255) NOT NULL, author VARCHAR(128) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smpost_party (smpost_id INT NOT NULL, party_id INT NOT NULL, INDEX IDX_4E45E42DB22C8286 (smpost_id), INDEX IDX_4E45E42D213C1059 (party_id), PRIMARY KEY(smpost_id, party_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sound (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, link VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_sound (tag_id INT NOT NULL, sound_id INT NOT NULL, INDEX IDX_DA62DA82BAD26311 (tag_id), INDEX IDX_DA62DA826AAA5C3E (sound_id), PRIMARY KEY(tag_id, sound_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, full_name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE venue (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, genre VARCHAR(100) NOT NULL, capacity INT DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE venue_tag (venue_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_43A922BC40A73EBA (venue_id), INDEX IDX_43A922BCBAD26311 (tag_id), PRIMARY KEY(venue_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C213C1059 FOREIGN KEY (party_id) REFERENCES party (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE party_tag ADD CONSTRAINT FK_7ACB0A50213C1059 FOREIGN KEY (party_id) REFERENCES party (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE party_tag ADD CONSTRAINT FK_7ACB0A50BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE smpost_party ADD CONSTRAINT FK_4E45E42DB22C8286 FOREIGN KEY (smpost_id) REFERENCES smpost (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE smpost_party ADD CONSTRAINT FK_4E45E42D213C1059 FOREIGN KEY (party_id) REFERENCES party (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_sound ADD CONSTRAINT FK_DA62DA82BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_sound ADD CONSTRAINT FK_DA62DA826AAA5C3E FOREIGN KEY (sound_id) REFERENCES sound (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE venue_tag ADD CONSTRAINT FK_43A922BC40A73EBA FOREIGN KEY (venue_id) REFERENCES venue (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE venue_tag ADD CONSTRAINT FK_43A922BCBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C213C1059');
        $this->addSql('ALTER TABLE party_tag DROP FOREIGN KEY FK_7ACB0A50213C1059');
        $this->addSql('ALTER TABLE smpost_party DROP FOREIGN KEY FK_4E45E42D213C1059');
        $this->addSql('ALTER TABLE smpost_party DROP FOREIGN KEY FK_4E45E42DB22C8286');
        $this->addSql('ALTER TABLE tag_sound DROP FOREIGN KEY FK_DA62DA826AAA5C3E');
        $this->addSql('ALTER TABLE party_tag DROP FOREIGN KEY FK_7ACB0A50BAD26311');
        $this->addSql('ALTER TABLE tag_sound DROP FOREIGN KEY FK_DA62DA82BAD26311');
        $this->addSql('ALTER TABLE venue_tag DROP FOREIGN KEY FK_43A922BCBAD26311');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('ALTER TABLE venue_tag DROP FOREIGN KEY FK_43A922BC40A73EBA');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE party');
        $this->addSql('DROP TABLE party_tag');
        $this->addSql('DROP TABLE smpost');
        $this->addSql('DROP TABLE smpost_party');
        $this->addSql('DROP TABLE sound');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE tag_sound');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE venue');
        $this->addSql('DROP TABLE venue_tag');
    }
}
